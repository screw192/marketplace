import React from "react";
import {Container, CssBaseline} from "@material-ui/core";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import {Route, Switch} from "react-router-dom";

import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import AddNewItem from "./containers/AddNewItem/AddNewItem";
import MarketplaceItems from "./containers/MarketplaceItems/MarketplaceItems";
import SingleItem from "./containers/SingleItem/SingleItem";


const App = () => {
  return (
    <>
      <CssBaseline/>
      <header>
        <AppToolbar/>
      </header>
      <main>
        <Container maxWidth="lg">
          <Switch>
            <Route path="/" exact component={MarketplaceItems} />
            <Route path="/login" exact component={Login} />
            <Route path="/register" exact component={Register} />
            <Route path="/add_item" exact component={AddNewItem} />
            <Route path="/items/:id" exact component={SingleItem} />
          </Switch>
        </Container>
      </main>
    </>
  );
}

export default App;