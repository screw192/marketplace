import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import userReducer from "./reducers/userReducer";
import categoriesReducer from "./reducers/categoriesReducer";
import marketplaceItemsReducer from "./reducers/MarketplaceItemsReducer";
import singleItemReducer from "./reducers/singleItemReducer";

const rootReducer = combineReducers({
  user: userReducer,
  categories: categoriesReducer,
  marketplaceItems: marketplaceItemsReducer,
  item: singleItemReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
  rootReducer,
  persistedState,
  composeEnhancers(applyMiddleware(thunkMiddleware))
);

store.subscribe(() => {
  saveToLocalStorage({
    user: store.getState().user
  });
});

export default store;