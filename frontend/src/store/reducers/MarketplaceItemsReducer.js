import {
  FETCH_ITEMS_FAILURE,
  FETCH_ITEMS_REQUEST,
  FETCH_ITEMS_SUCCESS
} from "../actions/MarketplaceItemsActions";

const initialState = {
  itemsLoading: true,
  itemsError: null,
  items: []
}

const marketplaceItemsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ITEMS_REQUEST:
      return {...state, itemsLoading: true};
    case FETCH_ITEMS_SUCCESS:
      return {...state, itemsLoading: false, items: action.marketplaceItems};
    case FETCH_ITEMS_FAILURE:
      return {...state, itemsLoading: false, itemsError: action.error};
    default:
      return state;
  }
};

export default marketplaceItemsReducer;