import {
  FETCH_SINGLE_ITEM_REQUEST,
  FETCH_SINGLE_ITEM_SUCCESS,
  FETCH_SINGLE_ITEM_FAILURE
} from "../actions/singleItemActions";

const initialState = {
  itemLoading: true,
  itemError: null,
  item: {}
};

const singleItemReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SINGLE_ITEM_REQUEST:
      return {...state, itemLoading: true};
    case FETCH_SINGLE_ITEM_SUCCESS:
      return {...state, itemLoading: false, item: action.itemData};
    case FETCH_SINGLE_ITEM_FAILURE:
      return {...state, itemLoading: false, itemError: action.error};
    default:
      return state;
  }
};

export default singleItemReducer;