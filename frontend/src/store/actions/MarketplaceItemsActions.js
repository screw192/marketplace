import axiosApi from "../../axiosApi";

export const CREATE_ITEM_SUCCES = "CREATE_ITEM";

export const FETCH_ITEMS_REQUEST = "FETCH_ITEMS_REQUEST";
export const FETCH_ITEMS_SUCCESS = "FETCH_ITEMS_SUCCESS";
export const FETCH_ITEMS_FAILURE = "FETCH_ITEMS_FAILURE";

export const createItemSuccess = () => ({type: CREATE_ITEM_SUCCES});

export const fetchItemsRequest = () => ({type: FETCH_ITEMS_REQUEST});
export const fetchItemsSuccess = marketplaceItems => ({type: FETCH_ITEMS_SUCCESS, marketplaceItems});
export const fetchItemsFailure = error => ({type: FETCH_ITEMS_FAILURE, error});

export const createItem = (itemData, token) => {
  return async dispatch => {
    await axiosApi.post("/items", itemData, {headers: {Authorization: token}});
    dispatch(createItemSuccess());
  };
};

export const fetchItems = () => {
  return async dispatch => {
    try {
      dispatch(fetchItemsRequest());

      const response = await axiosApi.get("/items");
      dispatch(fetchItemsSuccess(response.data));
    } catch (e) {
      dispatch(fetchItemsFailure(e));
    }
  };
};