import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_SINGLE_ITEM_REQUEST = "FETCH_SINGLE_ITEM_REQUEST";
export const FETCH_SINGLE_ITEM_SUCCESS = "FETCH_SINGLE_ITEM_SUCCESS";
export const FETCH_SINGLE_ITEM_FAILURE = "FETCH_SINGLE_ITEM_FAILURE";

export const DELETE_ITEM_SUCCESS = "DELETE_ITEM_SUCCESS";

export const fetchSingleItemRequest = () => ({type: FETCH_SINGLE_ITEM_REQUEST});
export const fetchSingleItemSuccess = itemData => ({type: FETCH_SINGLE_ITEM_SUCCESS, itemData});
export const fetchSingleItemFailure = error => ({type: FETCH_SINGLE_ITEM_FAILURE, error});

export const deleteItemSuccess = () => ({type: DELETE_ITEM_SUCCESS});

export const fetchSingleItem = itemID => {
  return async dispatch => {
    try {
      dispatch(fetchSingleItemRequest());

      const response = await axiosApi.get("/items/" + itemID);
      dispatch(fetchSingleItemSuccess(response.data));
    } catch (e) {
      dispatch(fetchSingleItemFailure(e));
    }
  };
};

export const deleteItem = (itemID, token) => {
  return async dispatch => {
    await axiosApi.delete("/items/" + itemID, {headers: {Authorization: token}});
    dispatch(deleteItemSuccess());
    dispatch(historyPush("/"));
  };
};