import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Grid, Typography} from "@material-ui/core";

import NewItemForm from "../../components/NewItemForm/NewItemForm";
import {fetchCategories} from "../../store/actions/categoriesActions";
import {historyPush} from "../../store/actions/historyActions";
import {createItem} from "../../store/actions/MarketplaceItemsActions";


const AddNewItem = () => {
  const dispatch = useDispatch();
  const categories = useSelector(state => state.categories.categories);
  const userData = useSelector(state => state.user.user);


  useEffect(() => {
    dispatch(fetchCategories());
  }, [dispatch]);

  const onProductFormSubmit = async (ItemData, token) => {
    await dispatch(createItem(ItemData, token));
    dispatch(historyPush("/"));
  };

  return (
      <Grid container direction="column" spacing={2}>
        <Grid item xs>
          <Typography variant="h4">Create new item</Typography>
        </Grid>
        <Grid item xs={6}>
          <NewItemForm onSubmit={onProductFormSubmit} categories={categories} user={userData} />
        </Grid>
      </Grid>
  );
};

export default AddNewItem;