import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Grid, makeStyles} from "@material-ui/core";

import {fetchItems} from "../../store/actions/MarketplaceItemsActions";
import {fetchCategories} from "../../store/actions/categoriesActions";
import ItemCard from "./ItemCard";


const useStyles = makeStyles({
  listCursor: {
    fontSize: "18px",
    cursor: "pointer",
    marginBottom: "10px",
    "&:hover": {
      color: "steelblue",
    }
  }
});

const MarketplaceItems = () => {
  const classes = useStyles();

  const dispatch = useDispatch();
  const categories = useSelector(state => state.categories.categories);
  const marketplaceItems = useSelector(state => state.marketplaceItems.items);

  const [category, setCategory] = useState(null);

  useEffect(() => {
    dispatch(fetchCategories());
  }, [dispatch]);

  useEffect(() => {
    dispatch(fetchItems());
  }, [dispatch]);

  const categoryHandler = categoryID => {
    setCategory(categoryID);
  };

  const categoryList = categories.map(cat => {
    return (
        <li
            key={cat.title}
            className={classes.listCursor}
            onClick={() => categoryHandler(cat._id)}
        >
          {cat.title}
        </li>
    );
  });

  let filteredArray;

  if (category) {
    filteredArray = marketplaceItems.filter(item => item.categoryID._id === category);
  } else {
    filteredArray = marketplaceItems;
  }

  const itemCards = filteredArray.map(item => {
    return (
        <ItemCard
            key={item._id}
            id={item._id}
            title={item.title}
            image={item.image}
            price={item.price}
        />
    );
  });

  return (
      <Grid container>
        <Grid item xs={3}>
          <ul>
            <li
                className={classes.listCursor}
                onClick={() => categoryHandler(null)}
            >
              All items
            </li>
            {categoryList}
          </ul>
        </Grid>
        <Grid item xs container spacing={2}>
          {itemCards}
        </Grid>
      </Grid>
  );
};

export default MarketplaceItems;