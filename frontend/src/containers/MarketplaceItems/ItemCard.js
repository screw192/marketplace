import React from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Card, CardActionArea, CardContent, CardMedia, Grid, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
  media: {
    height: 140,
  },
});

const ItemCard = ({id, image, title, price}) => {
  const classes = useStyles();


  return (
      <Grid item xs={3}>
        <Card >
          <CardActionArea component={Link} to={"/items/" + id}>
            <CardMedia
                className={classes.media}
                image={apiURL + "/" + image}
                title={title}
            />
            <CardContent>
              <Typography gutterBottom variant="h6" component="h2" noWrap>
                {title}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                ${price}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
  );
};

export default ItemCard;