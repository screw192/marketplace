import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";

import {deleteItem, fetchSingleItem} from "../../store/actions/singleItemActions";
import {Container, LinearProgress, Paper, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import Button from "@material-ui/core/Button";


const SingleItem = props => {
  const dispatch = useDispatch();
  const itemData = useSelector(state => state.item.item);
  const userData = useSelector(state => state.user.user);
  const itemLoading = useSelector(state => state.item.itemLoading);

  const itemID = props.match.params.id;

  useEffect(() => {
    dispatch(fetchSingleItem(itemID));
  }, [dispatch, itemID]);

  let removeButton = null;

  if (Object.keys(itemData).length !== 0) {
    if (itemData.userID._id === userData._id) {
      removeButton = (
          <Button
              variant="contained"
              color="secondary"
              onClick={() => dispatch(deleteItem(itemData._id, userData.token))}
          >
            Remove
          </Button>
      );
    }
  }

  return (
      <>
        {itemLoading ? (
            <LinearProgress />
          ) : (
              <Container maxWidth="md">
                <Grid component={Paper} variant="elevation" container spacing={2}>
                  <Grid item container spacing={2}>
                    <Grid item>
                      <img src={apiURL + "/" + itemData.image} alt={itemData.title} width="200px" height="auto"/>
                    </Grid>
                    <Grid item xs>
                      <Typography variant="h6">
                        {itemData.title} - ${itemData.price}
                      </Typography>
                      <Typography variant="body1">
                        {itemData.description}
                      </Typography>
                    </Grid>
                    <Grid item>
                      {removeButton}
                    </Grid>
                  </Grid>
                  <Grid item xs>
                    <Typography variant="h6">
                      Seller: {itemData.userID.displayName}
                    </Typography>
                    <Typography variant="h6">
                      Phone: +{itemData.userID.phoneNumber}
                    </Typography>
                  </Grid>
                </Grid>
              </Container>
          )
        }
      </>
  );
};

export default SingleItem;