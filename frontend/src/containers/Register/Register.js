import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import Container from "@material-ui/core/Container";
import {Avatar, Button, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

import {registerUser} from "../../store/actions/userActions";
import {Link as RouterLink} from "react-router-dom";
import FormElement from "../../components/UI/Form/FormElement";


const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  header: {
    marginBottom: theme.spacing(2)
  }
}));

const Register = () => {
  const classes = useStyles();

  const dispatch = useDispatch();
  const [user, setUser] = useState({
    username: "", password: "", displayName: "", phoneNumber: ""
  });

  const inputChangeHandler = e => {
    const {name, value} = e.target;

    setUser(prev => ({...prev, [name]: value}));
  };

  const submitFormHandler = e => {
    e.preventDefault();

    dispatch(registerUser({...user}));
  };

  return (
      <Container component="section" maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon/>
          </Avatar>
          <Typography component="h1" variant="h5" className={classes.header}>
            Sign up
          </Typography>
          <Grid container spacing={1} direction="column" component="form" onSubmit={submitFormHandler}>
            <FormElement
                label="Username"
                type="text"
                onChange={inputChangeHandler}
                name="username"
                value={user.username}
                autoComplete="new-username"
            />
            <FormElement
                label="Password"
                type="password"
                onChange={inputChangeHandler}
                name="password"
                value={user.password}
                autoComplete="new-password"
            />
            <FormElement
                label="Display name"
                type="text"
                onChange={inputChangeHandler}
                name="displayName"
                value={user.displayName}
                autoComplete="new-password"
            />
            <FormElement
                label="Phone number"
                type="text"
                onChange={inputChangeHandler}
                name="phoneNumber"
                value={user.phoneNumber}
                autoComplete="new-password"
            />
            <Grid item xs>
              <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  size="large"
                  color="primary"
                  className={classes.submit}
              >
                Sign up
              </Button>
            </Grid>
            <Grid item container justify="flex-end">
              <Grid item>
                <Link component={RouterLink} variant="body2" to="/login">
                  Already have an account?
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Container>
  );
};

export default Register;