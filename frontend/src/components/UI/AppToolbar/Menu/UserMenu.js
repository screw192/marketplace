import React from 'react';
import {useDispatch} from "react-redux";
import {Button, Typography} from "@material-ui/core";
import {logoutUser} from "../../../../store/actions/userActions";
import {Link} from "react-router-dom";

const UserMenu = ({user}) => {
  const dispatch = useDispatch();

  return (
      <>
        <Typography variant="body1" display="inline">
          Hello, <b>{user.displayName}</b>!
        </Typography>
        <Button
            component={Link}
            to="/add_item"
            color="inherit"
        >
          Add new item
        </Button>
        <Button
            onClick={() => dispatch(logoutUser())}
            color="inherit"
        >
          Logout
        </Button>
      </>
  );
};

export default UserMenu;