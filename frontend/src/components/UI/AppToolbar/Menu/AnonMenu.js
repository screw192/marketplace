import React from 'react';
import {Link} from "react-router-dom";
import {Button} from "@material-ui/core";

const AnonMenu = () => {
  return (
      <>
        <Button color="inherit" component={Link} to="/login">Login</Button>
        <Button color="inherit" component={Link} to="/register">Register</Button>
      </>
  );
};

export default AnonMenu;