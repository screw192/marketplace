const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Category = require("./models/Category");
const MarketItem = require("./models/MarketItem");
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user1, user2, user3, user4] = await User.create({
    username: 'user1',
    password: 'user1',
    displayName: "Michael de Santa",
    phoneNumber: 996555000001,
    token: nanoid()
  }, {
    username: 'user2',
    password: 'user2',
    displayName: "Franklin Clinton",
    phoneNumber: 996555000002,
    token: nanoid()
  }, {
    username: 'user3',
    password: 'user3',
    displayName: "Trevor Philips",
    phoneNumber: 996555000003,
    token: nanoid()
  }, {
    username: 'user4',
    password: 'user4',
    displayName: "Niko Bellic",
    phoneNumber: 996555000004,
    token: nanoid()
  });

  const [cat1, cat2, cat3, cat4] = await Category.create({
    title: "Shoes",
  }, {
    title: "Electronics",
  }, {
    title: "Clothes",
  }, {
    title: "Accessories",
  });

  await MarketItem.create({
    title: "Adibas sneakers",
    description: "Genuine leather, top quality, absolutely original",
    image: "fixtures/adibas.jpeg",
    price: 88,
    categoryID: cat1,
    userID: user4,
  }, {
    title: "Rolex wrist watch",
    description: "Gold plated 24k, good quality, has some scratches and dents",
    image: "fixtures/rolex.jpeg",
    price: 146,
    categoryID: cat4,
    userID: user1,
  }, {
    title: "Plasma TV 85",
    description: "Almost new, no documents and remote controller. Just TV!",
    image: "fixtures/tv.jpeg",
    price: 228,
    categoryID: cat2,
    userID: user2,
  }, {
    title: "Worn jeans",
    description: "Looks like new, worn couple times.",
    image: "fixtures/jeans.jpg",
    price: 14,
    categoryID: cat3,
    userID: user3,
  }, {
    title: "iPhone 6s",
    description: "Locked. Only for parts.",
    image: "fixtures/iphone.jpeg",
    price: 95,
    categoryID: cat2,
    userID: user2,
  }, {
    title: "Samsung Galaxu S21 ULTRA PRO EDISHEN",
    description: "New. Good quality. 5 days warranty.",
    image: "fixtures/samsung.jpeg",
    price: 1999,
    categoryID: cat2,
    userID: user4,
  });

  await mongoose.connection.close();
};

run().catch(console.error);