const express = require('express');
const path = require('path');
const multer = require('multer');
const {nanoid} = require('nanoid');
const MarketItem = require("../models/MarketItem");
const auth = require("../middleware/auth");
const config = require("../config");
const Category = require("../models/Category");
const User = require("../models/User");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({storage});

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const marketItems = await MarketItem
        .find()
        .select("title image price categoryID")
        .populate("categoryID", "title");

    res.send(marketItems);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const marketItem = await MarketItem
        .findOne({_id: req.params.id})
        .populate([
            {path: "categoryID", model: Category, select: "title"},
            {path: "userID", model: User, select: "displayName phoneNumber"}
        ]);

    res.send(marketItem);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", auth, upload.single('image'), async (req, res) => {
  try {
    const marketItemData = req.body;

    if (req.file) {
      marketItemData.image = 'uploads/' + req.file.filename;
    }

    marketItemData.userID = req.user._id;

    const marketItem = new MarketItem(marketItemData);
    await marketItem.save();

    res.send(marketItem);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete("/:id", auth, async (req, res) => {
  try {
    await MarketItem.deleteOne({_id: req.params.id})

    res.send({message: "Success"});
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;